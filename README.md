# Breakouts - Electronic Discovery

Just notes from my learning in electronic.

## Integrated circuits / IC

## Packages

IC expose pins/pads from the internal die in different form factor.

- `DIP` with long pins, the easier to use on `breadboards` or for manual soldering.
  
## Famous IC

### Real-time clocks (RTCs) & timers

- [NE555 - Single Precision Timer](https://www.ti.com/product/NE555#product-details)
- [SN7432 - Quad 2-input positive-OR gates](https://www.ti.com/product/SN7432)

## Prototyping

- `breadboard`: solderless, use with Dupont wire and `DIP` `IC`.

## Links

- [Integrated Circuits](https://learn.sparkfun.com/tutorials/integrated-circuits/all)
- [Circuit Simulator (HTML5/JS)](https://www.falstad.com/circuit/)
- [How To Use A Breadboard](https://www.build-electronic-circuits.com/breadboard/)