# traffic light

Reproduce traffic light example from [Circuit simulator](https://www.falstad.com/circuit/)

![img](images/circuit-trafficlight.png)

## Materials

- breadboard
- current source: `5V DC`
- `IC (DIP Package)`
  - __Timers__
    - `NE555`: 1
  - __Capacitors__:
    - `10 uF`: 1
  - __Resistors__
    - `47k`: 1
    - `10k`: 1
    - `200`: 2
  - __Gates__
    - _OR_:
      - `4->1`: 2
      - `2->1`: 2
  - __Leds__
    - `Red 10mA`: 2
    - `Green 10mA`: 2
    - `Yello 10mA`: 2
  