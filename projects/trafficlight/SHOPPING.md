# Shopping

## 31/01/2021
  
- [X] breadboard
- [X] current source: `5V DC` :checkmark:
- `IC (DIP Package)`
  - __Timers__
    - [X] `NE555`: 1
  - __Capacitors__:
    - [ ] `10 uF`: 1
  - __Resistors__
    - [X] `47k`: 1
    - [X] `10k`: 1
    - [X] `200`: 2
  - __Gates__
    - _OR_:
      - [X] `4->1`: 2 (SN74HC32N)
      - [X] `2->1`: 2
  - __Leds__
    - [X] `Red 10mA`: 2
    - [X] `Green 10mA`: 2
    - [X] `Yello 10mA`: 2
  